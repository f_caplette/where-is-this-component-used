# where-is-this-component-used

Have you ever looked at a component and thought: I don't know where this is rendered. This can happen often when looking at a codebase you are not familiar with and so this project is one that host a small utility to quickly tell you on which url a component is used.

## How to use it

Clone the repository to your machine.