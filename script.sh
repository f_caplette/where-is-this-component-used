#!/bin/bash

# TODO: Need users to define the root of the search in their GDK folder
START_DIR="/Users/fred/gitlab-development-kit/gitlab/app/assets/javascripts"
HAML_DIR="/Users/fred/gitlab-development-kit/gitlab/app/views"

FILES_QUEUE=()
SEEN_COMPONENTS=()

TAG_IDS=()
FILES=()

function findTag()
{
  # We are trying to find a tag element that
  # starts with `#js-` or `.js-` as mounting
  # elements typically starts with it
  TAG=$(grep -Eo '.?js-.+\w' $2)

  # If we cannot find the tag, we need to look
  # into the parent component and try again
  if [[ $TAG == '' ]]; then
    TAG=$(grep -Eo 'document.querySelector', $2)
    if [[ $TAG == '' ]]; then
      # we drop the `.js` from the file name since
      # we ling against JS imports having the file ext.
      findUsage ${1%.*} $2
    fi
  else
    TAG_IDS+=( $TAG )
  fi
}

function findUsage()
{
  # We get all the files where the desired component is used
  local FILES=( $(grep -R -l $1 $START_DIR ))

  local filtered_files=()
  for i in "${!FILES[@]}"
    do
      if [[ ! ${FILES[$i]} =~ $1 ]]; then
        filtered_files+=( ${FILES[$i]} )
      fi
    done
  
  # We want to keep only the component name to search import since
  # we may have relative imports.
  local components=("${filtered_files[@]##*/}")

  for i in "${!components[@]}"
    do
      echo -e "'$1' was used in '${filtered_files[*]##*javascripts/}' \n"
      if [[ ${filtered_files[$i]} =~ \.js$ ]]; then
        findTag ${filtered_files[$i]##*javascripts/} ${filtered_files[$i]}
      else
        # It's possible to have recursive vue components!
        # To avoid infinite loop, we remove any items that imports itself
        if [[ ${SEEN_COMPONENTS[@]} =~ ${components[$i]} ]]; then
          continue
        else
          SEEN_COMPONENTS+=(${components[$i]})
          findUsage "${components[$i]}" "${filtered_files[$i]}"
        fi
      fi
    done
}

function exec()
{
  if [ $1 ]; then
    echo "Initializing search for $1..."

    findUsage $1 ""
    # Find haml files that using the found tags
    for tag in "${TAG_IDS[@]}"
      do
        templates=( $(grep -R -l $tag $HAML_DIR ))
        echo "Resulting haml templates: ${templates[*]##*views/} for $tag"
      done
  else
    echo "You need to provide a valid component as an argument"
  fi
}

# Run script
exec $1